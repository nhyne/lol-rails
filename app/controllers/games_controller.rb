class GamesController < ApplicationController
  def index
    render json: Game.all,
           each_serializer: GameSerializer
  end

  def show
    render json: Game.find(params[:id])
  end

end
