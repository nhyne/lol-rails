class MatchesController < ApplicationController

  def index
    render json: Match.all,
           each_serializer: MatchSerializer
  end

  def show
    render json: Match.find(params[:id]),
           include: 'games,players'
  end

end
