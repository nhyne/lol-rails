class PlayersController < ApplicationController

  def index
    render json: Player.all,
           each_serializer: PlayerSerializer
  end

  def show
    render json: Player.find(params[:id])
  end

end
