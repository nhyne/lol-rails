class MatchDataFetcher < ActiveJob::Base
  def perform(summoner_name, region)
    get_matches_data(summoner_name, region)
  end


  def get_matches_data(summoner_name, region)
    player = Player.find_by(summoner_name: summoner_name)
    url = UrlGenerator::Factory.produce(:match, {data: player.account_id, type: :recent}).generate
    response = HTTParty.get(url)
    case response.code
      when 200
        return if Match.find_by(game_id: response.parsed_response['matches'].first['gameId'])
        response.parsed_response['matches'].each do |match|
          new_match = Match.create(game_id: match['gameId'])
          Game.create(champion_id: match['champion'], match_id: new_match.id, player_id: player.id)
        end
      when 403
        raise 'Unauthorized Error'
    end
  end


end
