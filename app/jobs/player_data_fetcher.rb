class PlayerDataFetcher < ActiveJob::Base
  def perform(summoner_name, region)
    summoner_insert(summoner_name, region)
  end

  def summoner_insert(summoner_name, region)
    user = Player.find_by(summoner_name: summoner_name)
    unless user
      url = UrlGenerator::Factory.produce(:summoner, {data: summoner_name, type: :by_name}).generate
      response = HTTParty.get(url)
      case response.code
        when 200
          repsonse = response.parsed_response
          user = Player.create(summoner_name: response['name'], account_id: response['accountId'], summoner_id: response['id'])
        else
         raise 'We did not get the data!' 
      end
    end
  end
  

end
