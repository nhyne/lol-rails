class Player < ApplicationRecord
  has_many :games
  has_many :matches, through: :games
end
