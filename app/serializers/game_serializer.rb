class GameSerializer < ActiveModel::Serializer
  attributes :id, :kills, :team, :assists, :deaths, :champion_id
  belongs_to :player, serializer: PlayerSerializer

end
