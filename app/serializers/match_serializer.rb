class MatchSerializer < ActiveModel::Serializer
  attributes :id, :winner, :red_kills, :blue_kills
  has_many :games, serializer: GameSerializer
  has_many :players, serializer: PlayerSerializer
end
