class PlayerSerializer < ActiveModel::Serializer
  attributes :id, :summoner_name
end
