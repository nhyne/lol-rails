class CreatePlayers < ActiveRecord::Migration[5.1]
  def change
    create_table :players do |t|
      t.string :summoner_name
      t.integer :account_id
      t.integer :summoner_id
      t.string :region
      t.timestamps
    end
  end
end
