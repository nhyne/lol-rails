class CreateMatches < ActiveRecord::Migration[5.1]
  def change
    create_table :matches do |t|
      t.boolean :winner
      t.integer :red_kills
      t.integer :blue_kills
      t.timestamps
    end
  end
end
