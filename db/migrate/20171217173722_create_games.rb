class CreateGames < ActiveRecord::Migration[5.1]
  def change
    create_table :games do |t|
      t.integer :match_id, null: false
      t.integer :player_id, null: false
      t.integer :kills, default: 0
      t.boolean :team, null: false
      t.integer :champion_id, null: false
      t.integer :kills, default: 0
      t.integer :assists, default: 0
      t.integer :deaths, default: 0
      t.timestamps
    end
  end
end
