class AddUniqueIndexToGames < ActiveRecord::Migration[5.1]
  def change
    add_index :games, [:player_id, :match_id], unique: true
  end
end
