class RemoveTeamFromGame < ActiveRecord::Migration[5.1]
  def change
    remove_column :games, :team
  end
end
