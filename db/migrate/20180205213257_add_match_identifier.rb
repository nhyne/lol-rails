class AddMatchIdentifier < ActiveRecord::Migration[5.1]
  def change
    add_column :matches, :game_id, :integer, null: false, limit: 8
  end
end
