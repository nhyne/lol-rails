module UrlGenerator
  class BaseGenerator
    def initialize(options)
      @options = options
    end

    def generate
      region = 'na1' || @options[:region]
      @base_url = "https://#{region}.api.riotgames.com/lol"
      self.sub_generate
      @base_url << "?api_key=#{Rails.application.secrets.api_key}"
    end
  end

end
