module UrlGenerator
  class Factory

    def self.produce(type, options)
      case type

        when :summoner
          return UrlGenerator::SummonerGenerator.new(options)
        when :match
          return UrlGenerator::MatchGenerator.new(options)
        else
          raise 'There is no default subclass.'
      end
    end

  end

end
