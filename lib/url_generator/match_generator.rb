module UrlGenerator
  class MatchGenerator < BaseGenerator
    def sub_generate
      @base_url << '/match/v3'
      case @options[:type]
        when :recent
          @base_url << "/matchlists/by-account/#{@options[:data]}/recent"
        when :match_id
          @base_url << "/matches/#{@options[:data]}"
        when :timeline
          @base_url << "/timelines/by-match/#{@options[:data]}"
        else
          raise 'no type given'
      end
    end
  end
end
