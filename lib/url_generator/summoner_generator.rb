module UrlGenerator
  class SummonerGenerator < BaseGenerator
    def sub_generate()
      @base_url << "/summoner/v3/summoners"
      case @options[:type]
        when :by_account
          @base_url << '/by-account'
        when :by_name
          @base_url << '/by-name'
        when :by_id
          @base_url << '/'
        else
          raise 'no type given'
      end
      @base_url << "/#{@options[:data]}"
    end
  end

end
